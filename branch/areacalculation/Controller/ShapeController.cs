﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using areacalculation.Model;

namespace areacalculation.Controller
{
    /// <summary>
    /// Interface of Controller
    /// </summary>
    public interface IShapeController
    {
        void OnChangeComboBox(int index);
        void OnClick(string sValue1, string sValue2, bool isSecondFieldVisible);
    };

    /// <summary>
    /// The controller process the user requests.
    /// Based on the user request, the Controller calls methods in the View and Model to accomplish the requested action. 
    /// </summary>
    class ShapeController : IShapeController
    {
        IShapeView view;
        IShapeModel model;

        public ShapeController(IShapeView sView, IShapeModel sModel)
        {
            this.view = sView;
            this.model = sModel;
            this.view.AddListener(this);
            this.model.InitPolygonAttribute();
            this.view.InitComboBoxValue(this.model.GetPolygonNames());
            this.view.ShowComboItem(0);
        }

        /// <summary>
        /// This method is responsible to update operation state of ShapeModel and assign 
        /// the inputput field according to operation state.
        /// </summary>
        /// <param name="cBox"></param>
        public void OnChangeComboBox(int index)
        {
            model.ChangeToState(index);
            view.UpdateTextField(model.GetSelectedPolygonAttribute());

        }

        /// <summary>
        /// OnClick is called when user click on calculate button. It parse the vaule of releted input 
        /// field. Call model by this value to calculate the result of selected polygon and update view result.
        /// Call model for previously calculated operations and set it to view's RichTextBox. 
        /// </summary>
        /// <param name="sValue1">first input field value</param>
        /// <param name="sValue2">second input field value</param>
        /// <param name="isSecondFieldVisible">second input field visibility status</param>
        public void OnClick(string sValue1, string sValue2, bool isSecondFieldVisible)
        {
            if ((sValue1 == "") || (sValue2 == "" && isSecondFieldVisible == true))
            {
                MessageBox.Show("Field value will never be empty");
                return;
            }
            float firstValue = float.Parse(sValue1);

            float secondValue = 0;
            if (isSecondFieldVisible == true)
                secondValue = float.Parse(sValue2);

            if (firstValue < 0 || secondValue < 0)
            {
                MessageBox.Show("Field value will never be negetive");
                return;
            }

            view.AreaRes = model.CalculateArea(firstValue, secondValue).ToString();
            view.OperationList = model.GetOperationList();
        }

    }
}
