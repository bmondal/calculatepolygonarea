﻿namespace areacalculation
{
	partial class ShapeView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.selectionLable = new System.Windows.Forms.Label();
            this.selectionComboBox = new System.Windows.Forms.ComboBox();
            this.firstLable = new System.Windows.Forms.Label();
            this.secondLable = new System.Windows.Forms.Label();
            this.firstTextBox = new System.Windows.Forms.TextBox();
            this.secondTextBox = new System.Windows.Forms.TextBox();
            this.calculate = new System.Windows.Forms.Button();
            this.area = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.list = new System.Windows.Forms.Label();
            this.finalListBox = new System.Windows.Forms.RichTextBox();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // selectionLable
            // 
            this.selectionLable.AutoSize = true;
            this.selectionLable.Location = new System.Drawing.Point(59, 41);
            this.selectionLable.Name = "selectionLable";
            this.selectionLable.Size = new System.Drawing.Size(64, 13);
            this.selectionLable.TabIndex = 0;
            this.selectionLable.Text = "Select Type";
            // 
            // selectionComboBox
            // 
            this.selectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectionComboBox.FormattingEnabled = true;
            this.selectionComboBox.Location = new System.Drawing.Point(137, 38);
            this.selectionComboBox.Name = "selectionComboBox";
            this.selectionComboBox.Size = new System.Drawing.Size(86, 21);
            this.selectionComboBox.TabIndex = 1;
            this.selectionComboBox.SelectedIndexChanged += new System.EventHandler(this.selectionComboBox_SelectedIndexChanged);
            // 
            // firstLable
            // 
            this.firstLable.AutoSize = true;
            this.firstLable.Location = new System.Drawing.Point(59, 93);
            this.firstLable.Name = "firstLable";
            this.firstLable.Size = new System.Drawing.Size(35, 13);
            this.firstLable.TabIndex = 2;
            this.firstLable.Text = "label1";
            // 
            // secondLable
            // 
            this.secondLable.AutoSize = true;
            this.secondLable.Location = new System.Drawing.Point(59, 123);
            this.secondLable.Name = "secondLable";
            this.secondLable.Size = new System.Drawing.Size(35, 13);
            this.secondLable.TabIndex = 3;
            this.secondLable.Text = "label1";
            // 
            // firstTextBox
            // 
            this.firstTextBox.Location = new System.Drawing.Point(137, 93);
            this.firstTextBox.Name = "firstTextBox";
            this.firstTextBox.Size = new System.Drawing.Size(86, 20);
            this.firstTextBox.TabIndex = 4;
            this.firstTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.firstTextBox_KeyPress);
            // 
            // secondTextBox
            // 
            this.secondTextBox.Location = new System.Drawing.Point(137, 123);
            this.secondTextBox.Name = "secondTextBox";
            this.secondTextBox.Size = new System.Drawing.Size(86, 20);
            this.secondTextBox.TabIndex = 5;
            this.secondTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.secondTextBox_KeyPress);
            // 
            // calculate
            // 
            this.calculate.Location = new System.Drawing.Point(137, 166);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(75, 23);
            this.calculate.TabIndex = 6;
            this.calculate.Text = "Calculate";
            this.calculate.UseVisualStyleBackColor = true;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // area
            // 
            this.area.AutoSize = true;
            this.area.Location = new System.Drawing.Point(59, 217);
            this.area.Name = "area";
            this.area.Size = new System.Drawing.Size(35, 13);
            this.area.TabIndex = 7;
            this.area.Text = "Area: ";
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Location = new System.Drawing.Point(137, 217);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(35, 13);
            this.result.TabIndex = 8;
            this.result.Text = "label1";
            // 
            // list
            // 
            this.list.AutoSize = true;
            this.list.Location = new System.Drawing.Point(59, 253);
            this.list.Name = "list";
            this.list.Size = new System.Drawing.Size(72, 13);
            this.list.TabIndex = 9;
            this.list.Text = "List of shapes";
            // 
            // finalListBox
            // 
            this.finalListBox.Location = new System.Drawing.Point(62, 279);
            this.finalListBox.Name = "finalListBox";
            this.finalListBox.ReadOnly = true;
            this.finalListBox.Size = new System.Drawing.Size(161, 102);
            this.finalListBox.TabIndex = 10;
            this.finalListBox.Text = "";
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(229, 38);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(59, 23);
            this.buttonRemove.TabIndex = 11;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // ShapeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 409);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.finalListBox);
            this.Controls.Add(this.list);
            this.Controls.Add(this.result);
            this.Controls.Add(this.area);
            this.Controls.Add(this.calculate);
            this.Controls.Add(this.secondTextBox);
            this.Controls.Add(this.firstTextBox);
            this.Controls.Add(this.secondLable);
            this.Controls.Add(this.firstLable);
            this.Controls.Add(this.selectionComboBox);
            this.Controls.Add(this.selectionLable);
            this.Name = "ShapeView";
            this.Text = "Polygon area calculation";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label selectionLable;
		private System.Windows.Forms.ComboBox selectionComboBox;
		private System.Windows.Forms.Label firstLable;
		private System.Windows.Forms.Label secondLable;
		private System.Windows.Forms.TextBox firstTextBox;
		private System.Windows.Forms.TextBox secondTextBox;
		private System.Windows.Forms.Button calculate;
		private System.Windows.Forms.Label area;
		private System.Windows.Forms.Label result;
        private System.Windows.Forms.Label list;
        private System.Windows.Forms.RichTextBox finalListBox;
        private System.Windows.Forms.Button buttonRemove;
	}
}

