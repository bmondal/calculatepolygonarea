﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace areacalculation.Model
{
    /// <summary>
    /// Interface of Model
    /// </summary>
    interface IShapeModel
    {
        void ChangeToState(int state);
        float CalculateArea(float number1, float number2);
        string GetOperationList();
        void InitPolygonAttribute();
        List<string> GetPolygonNames();
        List<string> GetSelectedPolygonAttribute();
    };

    /// <summary>
    /// The model for area calculation
    /// </summary>

    class ShapeModel : IShapeModel
    {

        float currentValue;
        List<Shape> shapeList = new List<Shape>();
        List<OperationString> operationDetails = new List<OperationString>();
        string curOpStr;
        string curOpName = "";
        int curSelection;

        /// <summary>
        /// Add primary polygon attribute to shapeList
        /// </summary>
        public void InitPolygonAttribute()
        {
            shapeList.Add(new Shape("Triangle", "Base", "Height"));
            shapeList.Add(new Shape("Rectangle", "Width", "Height"));
            shapeList.Add(new Shape("Square", "Length", ""));
            shapeList.Add(new Shape("Parallelogram", "Base", "Height"));
            shapeList.Add(new Shape("Circle", "Radius", ""));
        }

        /// <summary>
        /// Get all polygon name from shpeList for viewing in combo box. 
        /// </summary>
        /// <returns>return the list to controller</returns>
        public List<string> GetPolygonNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < shapeList.Count; i++)
                list.Add(shapeList[i].name);
            return list;
        }

        /// <summary>
        /// Retrive selected polygon's attributes.
        /// </summary>
        /// <returns>return the attributes list to controller</returns>
        public List<string> GetSelectedPolygonAttribute()
        {
            List<string> list = new List<string>();
            Shape selectedShape = shapeList[curSelection];
            list.Add(selectedShape.field1);
            list.Add(selectedShape.field2);
            return list;
        }

        /// <summary>
        /// Used to change operation state of model according to change of combo item.
        /// </summary>
        /// <param name="state">state receive changed index</param>
        public void ChangeToState(int index)
        {
            curSelection = index;
            this.curOpName = shapeList[index].name;
        }

        /// <summary>
        /// Calculate the area of last selected state.
        /// </summary>
        /// <param name="number1">value of first input field</param>
        /// <param name="number2">value of second input filed</param>
        /// <returns>calculated area</returns>
        public float CalculateArea(float number1, float number2)
        {
            if (curOpName == "Triangle")
            {
                curOpStr = "Triangle ( " + number1 + "," + number2 + " )";
                currentValue = AreaTriangle(number1, number2);
            }

            if (curOpName == "Rectangle")
            {
                curOpStr = "Rectangle ( " + number1 + "," + number2 + " )";
                currentValue = AreaRectangle(number1, number2);
            }

            if (curOpName == "Square")
            {
                curOpStr = "Square ( " + number1 + " )";
                currentValue = AreaSquare(number1);
            }
            if (curOpName == "Parallelogram")
            {
                curOpStr = "Parallelogram ( " + number1 + "," + number2 + " )";
                currentValue = AreaParallelogram(number1, number2);
            }

            if (curOpName == "Circle")
            {
                curOpStr = "Circle ( " + number1 + " )";
                currentValue = AreaCircle(number1);
            }

            return currentValue;
        }

        public float AreaTriangle(float value1, float value2)
        {
            return ((float)0.5 * value1 * value2);
        }

        public float AreaRectangle(float value1, float value2)
        {
            return (value1 * value2);
        }

        public float AreaSquare(float value1)
        {
            return (value1 * value1);
        }
        public float AreaParallelogram(float value1, float value2)
        {
            return (value1 * value2);
        }
        public float AreaCircle(float value1)
        {
            return ((float)Math.PI * value1 * value1);
        }

        /// <summary>
        /// For every operation,this method Add to List the operation details and sort it according to area.
        /// </summary>
        /// <returns>all previously calculated operation's string(sorted in accending order)</returns>
        public string GetOperationList()
        {
            string finalStr = "";
            operationDetails.Add(new OperationString(currentValue, curOpStr));
            operationDetails.Sort(delegate(OperationString r1, OperationString r2) { return r1.area.CompareTo(r2.area); });

            for (int i = 0; i < operationDetails.Count(); i++)
            {
                finalStr += operationDetails[i].opStr + "\r\n";
            }
            return finalStr;
        }

    }

    /// <summary>
    /// The Shape class used to  keep attribute of different shape
    /// </summary>
    public class Shape
    {
       // public int id { get; private set; }
        public string name { get; private set; }
        public string field1 { get; private set; }
        public string field2 { get; private set; }

        public Shape(string sName, string sField1, string sField2)
        {
           
            name = sName;
            field1 = sField1;
            field2 = sField2;
        }
    }


    /// <summary>
    /// Used to make an operation string with area.
    /// </summary>
    public class OperationString
    {
        public float area { get; private set; }
        public string opStr { get; private set; }

        public OperationString(float oId, string oStr)
        {
            area = oId;
            opStr = oStr;
        }
    }
}
