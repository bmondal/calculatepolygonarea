﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace areacalculation.Model
{
    /// <summary>
    /// Interface of Model
    /// </summary>
    interface IShapeModel
    {
        void ChangeToState(int state);
        float CalculateArea(float number1, float number2);
        string GetOperationList();
        void InitPolygonAttribute();
        void RemoveItem(int index);
        List<string> GetPolygonNames();
        List<string> GetSelectedPolygonAttribute();
    };

    /// <summary>
    /// The model for area calculation
    /// </summary>

    class ShapeModel : IShapeModel
    {

        float currentValue;
        List<Shape> shapeList = new List<Shape>();
        List<OperationString> operationDetails = new List<OperationString>();
        string curOpStr;
        string curOpName = "";
        int curSelection;

        /// <summary>
        /// Add primary polygon attribute to shapeList
        /// </summary>
        public void InitPolygonAttribute()
        {
            shapeList.Add(new Shape("Triangle", "Base", "Height"));
            shapeList.Add(new Shape("Rectangle", "Width", "Height"));
            shapeList.Add(new Shape("Square", "Length", ""));
            shapeList.Add(new Shape("Parallelogram", "Base", "Height"));
            shapeList.Add(new Shape("Circle", "Radius", ""));
            shapeList.Add(new Shape("Ellipse", "Radius1", "Radius2"));
            shapeList.Add(new Shape("Rhombus", "Diagonal1", "Diagonal2"));
        }

        /// <summary>
        /// Get all polygon name from shpeList for viewing in combo box. 
        /// </summary>
        /// <returns>return the list to controller</returns>
        public List<string> GetPolygonNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < shapeList.Count; i++)
                list.Add(shapeList[i].name);
            return list;
        }

        /// <summary>
        /// Retrive selected polygon's attributes.
        /// </summary>
        /// <returns>return the attributes list to controller</returns>
        public List<string> GetSelectedPolygonAttribute()
        {
            List<string> list = new List<string>();
            Shape selectedShape = shapeList[curSelection];
            list.Add(selectedShape.field1);
            list.Add(selectedShape.field2);
            return list;
        }

        /// <summary>
        /// Used to change operation state of model according to change of combo item.
        /// </summary>
        /// <param name="state">state receive changed index</param>
        public void ChangeToState(int index)
        {
            curSelection = index;
            this.curOpName = shapeList[index].name;
        }

        /// <summary>
        /// Calculate the area of last selected state.
        /// </summary>
        /// <param name="number1">value of first input field</param>
        /// <param name="number2">value of second input filed</param>
        /// <returns>calculated area</returns>
        public float CalculateArea(float number1, float number2)
        {
            if (curOpName == "Triangle")
            {
                curOpStr = "Triangle ( " + number1 + "," + number2 + " )";
                Triangle triangle = new Triangle(number1, number2);
                currentValue = triangle.Area(); // AreaTriangle(number1, number2);
            }

            if (curOpName == "Rectangle")
            {
                curOpStr = "Rectangle ( " + number1 + "," + number2 + " )";
                Rectangle rect = new Rectangle(number1, number2);
                currentValue = rect.Area();// AreaRectangle(number1, number2);
            }

            if (curOpName == "Square")
            {
                curOpStr = "Square ( " + number1 + " )";
                Square square = new Square(number1);
                currentValue = square.Area();//AreaSquare(number1);
            }
            if (curOpName == "Parallelogram")
            {
                curOpStr = "Parallelogram ( " + number1 + "," + number2 + " )";
                Parallelogram pg = new Parallelogram(number1, number2);
                currentValue = pg.Area();//AreaParallelogram(number1, number2);
            }

            if (curOpName == "Circle")
            {
                curOpStr = "Circle ( " + number1 + " )";
                Circle circle = new Circle(number1);
                currentValue = circle.Area();//AreaCircle(number1);
            }

            if (curOpName == "Ellipse")
            {
                curOpStr = "Ellipse ( " + number1 + "," + number2 + " )";
                Ellipse ellipse = new Ellipse(number1, number2);
                currentValue =ellipse.Area() ;//AreaEllipse(number1, number2);
            }

            if (curOpName == "Rhombus")
            {
                curOpStr = "Rhombus ( " + number1 + "," + number2 + " )";
                Rhombus rombus = new Rhombus(number1, number2);
                currentValue = rombus.Area();//AreaRhombus(number1,number2);
            }

            return currentValue;
        }

        public float AreaTriangle(float value1, float value2)
        {
            return ((float)0.5 * value1 * value2);
        }

        public float AreaRectangle(float value1, float value2)
        {
            return (value1 * value2);
        }

        public float AreaSquare(float value1)
        {
            return (value1 * value1);
        }
        public float AreaParallelogram(float value1, float value2)
        {
            return (value1 * value2);
        }
        public float AreaCircle(float value1)
        {
            return ((float)Math.PI * value1 * value1);
        }

        public float AreaEllipse(float value1, float value2)
        {
            return ((float)Math.PI * value1 * value2);
        }

        public float AreaRhombus(float value1, float value2)
        {
            return (float)(0.5*value1 * value2);
        }

        /// <summary>
        /// For every operation,this method Add to List the operation details and sort it according to area.
        /// </summary>
        /// <returns>all previously calculated operation's string(sorted in accending order)</returns>
        public string GetOperationList()
        {
            string finalStr = "";
            operationDetails.Add(new OperationString(currentValue, curOpStr));
            operationDetails.Sort(delegate(OperationString r1, OperationString r2) { return r1.area.CompareTo(r2.area); });

            for (int i = 0; i < operationDetails.Count(); i++)
            {
                finalStr += operationDetails[i].opStr + "\r\n";
            }
            return finalStr;
        }

        public void RemoveItem(int index)
        {
            shapeList.RemoveAt(index);
        }

    }

    /// <summary>
    /// The Shape class used to  keep attribute of different shape
    /// </summary>
    public class Shape
    {
       // public int id { get; private set; }
        public string name { get; private set; }
        public string field1 { get; private set; }
        public string field2 { get; private set; }

        public Shape(string sName, string sField1, string sField2)
        {
           
            name = sName;
            field1 = sField1;
            field2 = sField2;
        }
    }


    /// <summary>
    /// Used to make an operation string with area.
    /// </summary>
    public class OperationString
    {
        public float area { get; private set; }
        public string opStr { get; private set; }

        public OperationString(float oId, string oStr)
        {
            area = oId;
            opStr = oStr;
        }
    }

    public class Triangle
    {
        float basee;
        float height;
        public Triangle(float firstValue, float secondValue)
        {
            this.basee = firstValue;
            this.height = secondValue;
        }
        public float Area()
        {
            return ((float)0.5 * basee * height);
        }

    }

    public class Rectangle
    {
        float width;
        float height;
        public Rectangle(float firstValue, float secondValue)
        {
            this.width = firstValue;
            this.height = secondValue;
        }
        public float Area()
        {
            return (width * height);
        }

    }

    public class Square
    {
        float length;
        public Square(float firstValue)
        {
            this.length = firstValue;
        }
        public float Area()
        {
            return (length * length);
        }

    }

    public class Parallelogram
    {
        float basee;
        float height;
        public Parallelogram(float firstValue, float secondValue)
        {
            this.basee = firstValue;
            this.height = secondValue;
        }
        public float Area()
        {
            return (basee * height);
        }

    }

    public class Circle
    {
        float radious;
        public Circle(float firstValue)
        {
            this.radious = firstValue;
        }
        public float Area()
        {
            return ((float)Math.PI*radious * radious);
        }

    }

    public class Ellipse
    {
        float radious1;
        float radious2;
        public Ellipse(float firstValue,float secondValue)
        {
            this.radious1 = firstValue;
            this.radious2 = secondValue;
        }
        public float Area()
        {
            return ((float)Math.PI * radious1 * radious2);
        }
    }

    public class Rhombus
    {
        float Diagonal1;
        float Diagonal2;
        public Rhombus(float firstValue, float secondValue)
        {
            this.Diagonal1 = firstValue;
            this.Diagonal2 = secondValue;
        }
        public float Area()
        {
            return ((float)0.5 * Diagonal1 * Diagonal2);
        }
    }
}
