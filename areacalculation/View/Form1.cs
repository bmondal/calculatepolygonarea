﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using areacalculation.Model;
using areacalculation.Controller;

namespace areacalculation
{
    /// <summary>
    /// All interaction with the areacalculation's view should go through here.
    /// </summary>
    interface IShapeView
    {

        void AddListener(IShapeController controller);
        void ShowComboItem(int index);
        void UpdateTextField(List<string> fieldsName);
        void InitComboBoxValue(List<string> list);
        void HideAll();
        string AreaRes
        {
            get;
            set;
        }

        string OperationList
        {
            get;
            set;
        }
    };

   

    

    /// <summary>
    /// Windows Form .
    /// 
    /// </summary>

    public partial class ShapeView : Form, IShapeView
    {
        IShapeController controller;
        public ShapeView()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Initialize the combobox item
        /// </summary>

        public void InitComboBoxValue(List<string> list)
        {
            selectionComboBox.Items.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                selectionComboBox.Items.Add(list[i]);
            }
            result.Text = "";
        }

        /// <summary>
        /// set controller
        /// </summary>
        /// <param name="sController"></param>
        public void AddListener(IShapeController sController)
        {
            this.controller = sController;
        }

        /// <summary>
        /// Set an index as a selected item inside combobox.
        /// </summary>
        /// <param name="index">selected item index</param>
        public void ShowComboItem(int index)
        {
            selectionComboBox.SelectedIndex = index;
        }

        /// <summary>
        /// This method is used to changed number of input field according to polygon type and reset input field&result. 
        /// </summary>
        /// <param name="selectedIndex">index of selected combo box item</param>
        public void UpdateTextField(List<string> fieldsName)
        {

            firstTextBox.Text = "";
            secondTextBox.Text = "";
            result.Text = "";
            firstLable.Text = fieldsName[0];  // need to change variable
            secondLable.Text = fieldsName[1];
            if (secondLable.Text == "")
            {
                secondTextBox.Hide();
                secondLable.Hide();
            }
            else
            {
                secondTextBox.Show();
                secondLable.Show();
            }

        }


        public void HideAll()
        {
            firstLable.Hide();
            firstTextBox.Hide();
            secondTextBox.Hide();
            secondLable.Hide();
        }

        /// <summary>
        /// used to update result field value
        /// </summary>
        public string AreaRes
        {
            get
            {
                return result.Text;
            }
            set
            {
                result.Text = value;
            }
        }

        /// <summary>
        /// used to update view of previously calculated oparations.
        /// </summary>
        public string OperationList
        {
            get
            {
                return finalListBox.Text;
            }
            set
            {
                finalListBox.Text = value;
            }
        }

        /// <summary>
        /// Event handler of calculate button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void calculate_Click(object sender, EventArgs e)
        {
            controller.OnClick(firstTextBox.Text, secondTextBox.Text, secondTextBox.Visible);
        }

        /// <summary>
        /// Combo box item changed handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            controller.OnChangeComboBox(selectionComboBox.SelectedIndex);
        }

        /// <summary>
        /// Implemented to limit input field between integer & float.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void firstTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Implemented to limit input field between integer & float.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void secondTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            controller.OnRemove(selectionComboBox.SelectedIndex);
        }


    }

    
}
