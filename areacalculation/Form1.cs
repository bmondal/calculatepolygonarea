﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace areacalculation
{
    /// <summary>
    /// All interaction with the areacalculation's view should go through here.
    /// </summary>
    interface IShapeView
    {

        void AddListener(IShapeController controller);
        void ShowComboItem(int index);
        void UpdateTextField(List<string> fieldsName);
        void initComboBoxValue(List<string> list);
        string AreaRes
        {
            get;
            set;
        }

        string OperationList
        {
            get;
            set;
        }
    };

    /// <summary>
    /// Interface of Controller
    /// </summary>
    public interface IShapeController
    {
        void OnChangeComboBox(ComboBox cBox);
        void OnClick(string sValue1, string sValue2, bool isSecondFieldVisible);
    };

    /// <summary>
    /// Interface of Model
    /// </summary>
    interface IShapeModel
    {
        void ChangeToState(int state);
        float CalculateArea(float number1, float number2);
        string GetOperationList();
        void InitPolygonAttribute();
        List<string> GetPolygonNames();
        List<string> GetSelectedPolygonAttribute();
    };

    /// <summary>
    /// Windows Form .
    /// 
    /// </summary>

    public partial class ShapeView : Form, IShapeView
    {
        IShapeController controller;
        public ShapeView()
        {
            InitializeComponent();
            //initComboBoxValue();
        }
        /// <summary>
        /// Initialize the combobox item
        /// </summary>

        public void initComboBoxValue(List<string> list)
        {

            for (int i = 0; i < list.Count; i++)
            {
                selectionComboBox.Items.Add(new ComboItem(i, list[i]));
            }
            result.Text = "";
        }

        /// <summary>
        /// set controller
        /// </summary>
        /// <param name="sController"></param>
        public void AddListener(IShapeController sController)
        {
            this.controller = sController;
        }

        /// <summary>
        /// Set an index as a selected item inside combobox.
        /// </summary>
        /// <param name="index">selected item index</param>
        public void ShowComboItem(int index)
        {
            selectionComboBox.SelectedIndex = index;
        }

        /// <summary>
        /// This method is used to changed number of input field according to polygon type and reset input field&result. 
        /// </summary>
        /// <param name="selectedIndex">index of selected combo box item</param>
        public void UpdateTextField(List<string> fieldsName)
        {

            firstTextBox.Text = "";
            secondTextBox.Text = "";
            result.Text = "";
            firstLable.Text = fieldsName[0];  // need to change variable
            secondLable.Text = fieldsName[1];
            if (secondLable.Text == "")
            {
                secondTextBox.Hide();
                secondLable.Hide();
            }
            else
            {
                secondTextBox.Show();
                secondLable.Show();
            }

        }

        /// <summary>
        /// used to update result field value
        /// </summary>
        public string AreaRes
        {
            get
            {
                return result.Text;
            }
            set
            {
                result.Text = value;
            }
        }

        /// <summary>
        /// used to update view of previously calculated oparations.
        /// </summary>
        public string OperationList
        {
            get
            {
                return finalListBox.Text;
            }
            set
            {
                finalListBox.Text = value;
            }
        }

        /// <summary>
        /// Event handler of calculate button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void calculate_Click(object sender, EventArgs e)
        {
            controller.OnClick(firstTextBox.Text, secondTextBox.Text, secondTextBox.Visible);
        }

        /// <summary>
        /// Combo box item changed handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            controller.OnChangeComboBox((ComboBox)sender);
        }

        /// <summary>
        /// Implemented to limit input field between integer & float.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void firstTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Implemented to limit input field between integer & float.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void secondTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }


    }

    /// <summary>
    /// The controller process the user requests.
    /// Based on the user request, the Controller calls methods in the View and Model to accomplish the requested action. 
    /// </summary>
    class ShapeController : IShapeController
    {
        IShapeView view;
        IShapeModel model;

        public ShapeController(IShapeView sView, IShapeModel sModel)
        {
            this.view = sView;
            this.model = sModel;
            this.view.AddListener(this);
            this.model.InitPolygonAttribute();
            this.view.initComboBoxValue(this.model.GetPolygonNames());
            this.view.ShowComboItem(0);
        }

        /// <summary>
        /// This method is responsible to update operation state of ShapeModel and assign 
        /// the inputput field according to operation state.
        /// </summary>
        /// <param name="cBox"></param>
        public void OnChangeComboBox(ComboBox cBox)
        {
            ComboItem Item = (ComboItem)cBox.SelectedItem;
            model.ChangeToState(Item.id);
            view.UpdateTextField(model.GetSelectedPolygonAttribute());

        }

        /// <summary>
        /// OnClick is called when user click on calculate button. It parse the vaule of releted input 
        /// field. Call model by this value to calculate the result of selected polygon and update view result.
        /// Call model for previously calculated operations and set it to view's RichTextBox. 
        /// </summary>
        /// <param name="sValue1">first input field value</param>
        /// <param name="sValue2">second input field value</param>
        /// <param name="isSecondFieldVisible">second input field visibility status</param>
        public void OnClick(string sValue1, string sValue2, bool isSecondFieldVisible)
        {
            if ((sValue1 == "") || (sValue2 == "" && isSecondFieldVisible == true))
            {
                MessageBox.Show("Field value will never be empty");
                return;
            }
            float firstValue = float.Parse(sValue1);

            float secondValue = 0;
            if (isSecondFieldVisible == true)
                secondValue = float.Parse(sValue2);

            if (firstValue < 0 || secondValue < 0)
            {
                MessageBox.Show("Field value will never be negetive");
                return;
            }

            view.AreaRes = model.CalculateArea(firstValue, secondValue).ToString();
            view.OperationList = model.GetOperationList();
        }

    }

    /// <summary>
    /// The model for area calculation
    /// </summary>

    class ShapeModel : IShapeModel
    {

        public enum States { Triangle, Rectangle, Square, Parallelogram, Circle };
        States state;
        float currentValue;
        List<Shape> shapeList = new List<Shape>();
        List<OperationString> operationDetails = new List<OperationString>();
        string curOpStr;
        public States State
        {
            set { state = value; }
        }

        public void InitPolygonAttribute()
        {
            shapeList.Add(new Shape(0, "Triangle", "Base", "Height"));
            shapeList.Add(new Shape(1, "Rectangle", "Width", "Height"));
            shapeList.Add(new Shape(2, "Square", "Length", ""));
            shapeList.Add(new Shape(3, "Parallelogram", "Base", "Height"));
            shapeList.Add(new Shape(4, "Circle", "Radius", ""));
        }

        public List<string> GetPolygonNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < shapeList.Count; i++)
                list.Add(shapeList[i].name);
            return list;
        }

        public List<string> GetSelectedPolygonAttribute()
        {
            List<string> list = new List<string>();
            Shape selectedShape = shapeList[(int)state];
            list.Add(selectedShape.field1);
            list.Add(selectedShape.field2);
            return list;
        }

        /// <summary>
        /// Used to change operation state of model according to change of combo item.
        /// </summary>
        /// <param name="state">state receive changed index</param>
        public void ChangeToState(int state)
        {
            if (state == 0)
                this.state = States.Triangle;
            if (state == 1)
                this.state = States.Rectangle;
            if (state == 2)
                this.state = States.Square;
            if (state == 3)
                this.state = States.Parallelogram;
            if (state == 4)
                this.state = States.Circle;
        }

        /// <summary>
        /// Calculate the area of last selected state.
        /// </summary>
        /// <param name="number1">value of first input field</param>
        /// <param name="number2">value of second input filed</param>
        /// <returns>calculated area</returns>
        public float CalculateArea(float number1, float number2)
        {
            if (state == States.Triangle)
            {
                curOpStr = "Triangle ( " + number1 + "," + number2 + " )";
                currentValue = AreaTriangle(number1, number2);
            }

            if (state == States.Rectangle)
            {
                curOpStr = "Rectangle ( " + number1 + "," + number2 + " )";
                currentValue = AreaRectangle(number1, number2);
            }

            if (state == States.Square)
            {
                curOpStr = "Square ( " + number1 + " )";
                currentValue = AreaSquare(number1);
            }
            if (state == States.Parallelogram)
            {
                curOpStr = "Parallelogram ( " + number1 + "," + number2 + " )";
                currentValue = AreaParallelogram(number1, number2);
            }

            if (state == States.Circle)
            {
                curOpStr = "Circle ( " + number1 + " )";
                currentValue = AreaCircle(number1);
            }

            return currentValue;
        }

        public float AreaTriangle(float value1, float value2)
        {
            return ((float)0.5 * value1 * value2);
        }

        public float AreaRectangle(float value1, float value2)
        {
            return (value1 * value2);
        }

        public float AreaSquare(float value1)
        {
            return (value1 * value1);
        }
        public float AreaParallelogram(float value1, float value2)
        {
            return (value1 * value2);
        }
        public float AreaCircle(float value1)
        {
            return ((float)Math.PI * value1 * value1);
        }

        /// <summary>
        /// For every operation,this method Add to List the operation details and sort it according to area.
        /// </summary>
        /// <returns>all previously calculated operation's string(sorted in accending order)</returns>
        public string GetOperationList()
        {
            string finalStr = "";
            operationDetails.Add(new OperationString(currentValue, curOpStr));
            operationDetails.Sort(delegate(OperationString r1, OperationString r2) { return r1.area.CompareTo(r2.area); });

            for (int i = 0; i < operationDetails.Count(); i++)
            {
                finalStr += operationDetails[i].opStr + "\r\n";
            }
            return finalStr;
        }

    }

    /// <summary>
    /// The Shape class used to  keep attribute of different shape
    /// </summary>
    public class Shape
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string field1 { get; private set; }
        public string field2 { get; private set; }

        public Shape(int sId, string sName, string sField1, string sField2)
        {
            id = sId;
            name = sName;
            field1 = sField1;
            field2 = sField2;
        }
    }

    /// <summary>
    /// The ComboItem class used to keep ComboBox item.
    /// </summary>
    public class ComboItem
    {
        public int id { get; private set; }
        public string name { get; private set; }

        public ComboItem(int cId, string cName)
        {
            id = cId;
            name = cName;
        }

        public override string ToString()
        {
            return name;
        }
    }

    /// <summary>
    /// Used to make an operation string with area.
    /// </summary>
    public class OperationString
    {
        public float area { get; private set; }
        public string opStr { get; private set; }

        public OperationString(float oId, string oStr)
        {
            area = oId;
            opStr = oStr;
        }
    }

}
