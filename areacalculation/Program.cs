﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using areacalculation.Model;
using areacalculation.Controller;
namespace areacalculation
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			ShapeView view = new ShapeView();
            ShapeModel model = new ShapeModel();
            ShapeController controller = new ShapeController(view, model);
			Application.Run(view);
		}
	}
}
